//
//  NetworkDataFetcher.swift
//  VkWall
//
//  Created by Hewnon Wisp on 17/09/2019.
//  Copyright © 2019 Алексей Маринин. All rights reserved.
//

import Foundation

protocol DataFetcher {
    func getFedd(response: @escaping (FeedResponse?) -> Void)
    
    func getUser(response: @escaping (UserResponse?) -> Void)
}

struct NetworkDataFetcher: DataFetcher {
    
    private let authService: AuthService
    let networking: Networking
    
    init(networking: Networking, authService: AuthService = AppDelegate.shared().authService) {
        self.networking = networking
        self.authService = authService
    }
    
    func getUser(response: @escaping (UserResponse?) -> Void) {
        guard let userId = authService.userId else { return }
        let params = ["fields": "photo_100", "user_ids": userId]
        networking.request(path: API.profile, params: params) { (data, error) in
            if let error = error {
                print("Error received requesting data: \(error.localizedDescription)")
                response(nil)
            }
            
            let decoded = self.decodeJSON(type: UserResponseWrapped.self, from: data)
            response(decoded?.response.first)
        }
    }

    func getFedd(response: @escaping (FeedResponse?) -> Void) {
        let params = ["filters": "all", "count": "100"]
        networking.request(path: API.wallFeed, params: params) { (data, error) in
            if let error = error {
                print("Error received requesting data: \(error.localizedDescription)")
                response(nil)
            }
            let decoded = self.decodeJSON(type: FeedResponseWrapped.self, from: data)
            response(decoded?.response)
        }
}
    private func decodeJSON<T: Decodable>(type: T.Type, from: Data?) -> T? {
        let decoder = JSONDecoder()
        guard let data = from, let response = try? decoder.decode(type.self, from: data) else { return nil }
        return response
    }
}
