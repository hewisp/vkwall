//
//  FeedResponse.swift
//  VkWall
//
//  Created by Hewnon Wisp on 16/09/2019.
//  Copyright © 2019 Алексей Маринин. All rights reserved.
//

import Foundation

struct FeedResponseWrapped: Decodable {
    let response: FeedResponse
}

struct FeedResponse: Decodable {
    var items: [FeedItem]
}

struct FeedItem: Decodable {
    let owner_id: Int
    let from_id: Int
    let text: String?
    let post_type: String
    let date: Double
    let comments: CountableItem?
    let likes: CountableItem?
    let reposts: CountableItem?
    let views: CountableItem?
    var attachments: [Attechment]?
    var copy_history: [CopyHistory]?
}

struct Attechment: Decodable {
    let photo: Photo?
}

struct  Photo: Decodable {
    let sizes: [PhotoSize]
    
    var height: Int {
        return getPropperSize().height
    }
    var width: Int {
        return getPropperSize().width
    }
    var srcBIG: String {
        return getPropperSize().url
    }
    
    private func getPropperSize() -> PhotoSize {
        if let sizeX = sizes.first(where: { $0.type == "x" }) {
            return sizeX
        } else if let fallBackSize = sizes.last {
            return fallBackSize
        } else {
            return PhotoSize(type: "wrong image", url: "wrong image", width: 0, height: 0)
        }
    }
}

struct PhotoSize: Decodable {
    let type: String
    let url: String
    let width: Int
    let height: Int
}

struct CopyHistory: Decodable {
    let id : Int
    let text: String?
    let attachments: [Attechment]?
}

struct CountableItem: Decodable {
    let count: Int
}
