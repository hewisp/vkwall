//
//  Profile.swift
//  VkWall
//
//  Created by Hewnon Wisp on 18/09/2019.
//  Copyright © 2019 Алексей Маринин. All rights reserved.
//

import Foundation

struct UserResponseWrapped: Decodable {
    let response: [UserResponse]
}

struct UserResponse: Decodable {
    let photo_100: String?
    let first_name: String
    let last_name: String
}
