//
//  API.swift
//  VkWall
//
//  Created by Hewnon Wisp on 15/09/2019.
//  Copyright © 2019 Алексей Маринин. All rights reserved.
//

import Foundation

struct API {
    static let scheme = "https"
    static let host = "api.vk.com"
    static let version = "5.101"
    
    static let wallFeed = "/method/wall.get"
    static let profile = "/method/users.get"
    static let post = "/method/wall.post"
}
