//
//  PostToTheWall.swift
//  VkWall
//
//  Created by Hewnon Wisp on 20/09/2019.
//  Copyright © 2019 Алексей Маринин. All rights reserved.
//

import Foundation
import UIKit

class PostInWall: UIViewController {
    
    @IBOutlet weak var text: UITextView!
    
    @IBAction func buttomToSend(_ sender: UIButton) {
        if text.text.count == 0 { return }
        let authService: AuthService = AppDelegate.shared().authService
        
        let token = authService.token
        var allParams = ["message" : String(text.text), "owner_id" : String(owner_id)]
        allParams["access_token"] = token
        allParams["v"] = API.version
        
        
        let url = self.url(from: API.post, params: allParams)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let session = URLSession.shared
        
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            
            guard let data = data else { return }
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                print(json)
            } catch {
                print(error)
            }
        }.resume()
    }
    
private func url(from path: String, params: [String: String]) -> URL {
        var components = URLComponents()
        components.scheme = API.scheme
        components.host = API.host
        components.path = path
        components.queryItems = params.map{ URLQueryItem(name: $0, value: $1) }
        return components.url!
    }
}
