//
//  WallInteractor.swift
//  VkWall
//
//  Created by Hewnon Wisp on 17/09/2019.
//  Copyright (c) 2019 Алексей Маринин. All rights reserved.
//

import UIKit

protocol WallBusinessLogic {
  func makeRequest(request: Wall.Model.Request.RequestType)
}

class WallInteractor: WallBusinessLogic {

  var presenter: WallPresentationLogic?
  var service: WallService?
    
    private var fetcher: DataFetcher = NetworkDataFetcher(networking: NetworkService())
  
  func makeRequest(request: Wall.Model.Request.RequestType) {
    if service == nil {
      service = WallService()
    }
    
    switch request {
    
    case .getWallFedd:
        fetcher.getFedd(response: { [weak self] (feedResponse) in
            
            feedResponse?.items.map({ (from_id) in
                print("\(from_id)\n\n")
            })
            
            guard let feedResponse = feedResponse else { return }
            self?.presenter?.presentData(response: Wall.Model.Response.ResponseType.presentWall(feed: feedResponse))
        })
    case .getUser:
        fetcher.getUser{ (userResponse) in
            print(userResponse)
        }
    }
    }
  
}
