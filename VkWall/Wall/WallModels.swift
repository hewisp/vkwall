//
//  WallModels.swift
//  VkWall
//
//  Created by Hewnon Wisp on 17/09/2019.
//  Copyright (c) 2019 Алексей Маринин. All rights reserved.
//

import UIKit

enum Wall {
   
  enum Model {
    struct Request {
      enum RequestType {
        case getWallFedd
        case getUser
      }
    }
    struct Response {
      enum ResponseType {
        case presentWall(feed: FeedResponse)
      }
    }
    struct ViewModel {
      enum ViewModelData {
        case displayWall(feedViewModel: FeedViewModel)
      }
    }
  }
}

struct FeedViewModel {
    struct Cell: FeedCellViewModel {
        var iconUrlString: String
        var name: String
        var date: String
        var text: String?
        var likes: String?
        var comments: String?
        var shares: String?
        var views: String?
        var photoAttachment: FeedCellPhotoAttechementViewModel?
        var sizes: FeedCellSizes
    }
    
    struct FeedCellPhotoAttachment: FeedCellPhotoAttechementViewModel/*FeedCellPhotoAttechementViewModel*/ {
        var prhotoUrlString: String?
        var width: Int
        var height: Int
    }
    
    let cells: [Cell]
}
