//
//  WallCellLoyoutCalculator.swift
//  VkWall
//
//  Created by Hewnon Wisp on 19/09/2019.
//  Copyright © 2019 Алексей Маринин. All rights reserved.
//

import Foundation
import UIKit

struct Sizes: FeedCellSizes {
    var postLabelFrame: CGRect
    var attachmentFrame: CGRect
    var bottomViewFrame: CGRect
    var totalHeight: CGFloat
}

struct Constants {
    static let cardInsets = UIEdgeInsets(top: 0, left: 7, bottom: 10, right: 8)
    static let topViewHeight: CGFloat = 35
    static let postLavelInsets = UIEdgeInsets(top: 7 + Constants.topViewHeight + 8, left: 7, bottom: 8, right: 8)
    static let postLabelFont = UIFont.systemFont(ofSize: 15)
    static let bottomViewHeight: CGFloat = 39

}

protocol FeedCellLayoutCalculatorProtocol {
    func sizes(postText: String?, photoAttachment: FeedCellPhotoAttechementViewModel?) -> FeedCellSizes
}

final class FeedCellLayaoutCalculator: FeedCellLayoutCalculatorProtocol {

    private let screenWidth: CGFloat

    init(screenWidth: CGFloat = min(UIScreen.main.bounds.width, UIScreen.main.bounds.height)) {
        self.screenWidth = screenWidth
    }

    func sizes(postText: String?, photoAttachment: FeedCellPhotoAttechementViewModel?) -> FeedCellSizes {
        let cardViewWidth = screenWidth - Constants.cardInsets.left - Constants.cardInsets.right

        //MARK: работа с постлабелфрейм

        var postLabelFrame = CGRect(origin: CGPoint(x: Constants.postLavelInsets.left, y: Constants.postLavelInsets.top),
                                    size: CGSize.zero)

        if let text = postText, !text.isEmpty {
            let width = cardViewWidth - Constants.postLavelInsets.left - Constants.postLavelInsets.right
            let height = text.height(width: width, font: Constants.postLabelFont)

            postLabelFrame.size = CGSize(width: width, height: height)
        }

        // работа с атечментфрейм

        let attachmentTop = postLabelFrame.size == CGSize.zero ? Constants.postLavelInsets.top : postLabelFrame.maxY + Constants.postLavelInsets.bottom

        var attachmentFrame = CGRect(origin: CGPoint(x: 0, y: attachmentTop), size: CGSize.zero)

        if let attachment = photoAttachment {
            let photoHeight: Float = Float(attachment.height)
            let photoWidth: Float = Float(attachment.width)

            let ratio = CGFloat(photoHeight / photoWidth)
            attachmentFrame.size = CGSize(width: cardViewWidth, height: cardViewWidth * ratio)
        }

        //работа с баттомВью
        let bittomViewTop = max(postLabelFrame.maxY, attachmentFrame.maxY)

        let bottomViewFrame = CGRect(origin: CGPoint(x: 0, y: bittomViewTop), size: CGSize(width: cardViewWidth, height: Constants.bottomViewHeight))

        let totalHeight = bottomViewFrame.maxY + Constants.cardInsets.bottom

        return Sizes(postLabelFrame: postLabelFrame,
                     attachmentFrame: attachmentFrame,
                     bottomViewFrame: bottomViewFrame,
                     totalHeight: totalHeight)
    }

}
