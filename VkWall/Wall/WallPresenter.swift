//
//  WallPresenter.swift
//  VkWall
//
//  Created by Hewnon Wisp on 17/09/2019.
//  Copyright (c) 2019 Алексей Маринин. All rights reserved.
//

import UIKit

var owner_id: Int = Int()

protocol WallPresentationLogic {
  func presentData(response: Wall.Model.Response.ResponseType)
}

class WallPresenter: WallPresentationLogic {
    
    
  weak var viewController: WallDisplayLogic?
    var cellLayoutCalculator: FeedCellLayoutCalculatorProtocol = FeedCellLayaoutCalculator()
    
    let dateFormatter: DateFormatter = {
       let dt = DateFormatter()
        dt.locale = Locale(identifier: "ru_RU")
        dt.dateFormat = "d MMM 'в' HH:mm"
        return dt
    }()
    
    
  func presentData(response: Wall.Model.Response.ResponseType) {
    switch response {
    case .presentWall(let feed):
    
        let cells = feed.items.map({ (feedItem) in
            cellViewModel(from: feedItem)
        })
        
        let feedViewModel = FeedViewModel.init(cells: cells)
        viewController?.displayData(viewModel: Wall.Model.ViewModel.ViewModelData.displayWall(feedViewModel: feedViewModel))
        
    }
  }
    
    private func cellViewModel(from feedItem: FeedItem) -> FeedViewModel.Cell {
        var name = "id: " + String(feedItem.from_id)
        var photo = ""
//        let authService: AuthService = AppDelegate.shared().authService
//
//        let token = authService.token
//        var allParams = ["fields": "photo_100", "user_ids": String(feedItem.from_id)]
//        allParams["access_token"] = token
//        allParams["v"] = API.version
//        sleep(1)
//        let url = self.url(from: API.profile, params: allParams)
//        let session = URLSession.shared
//        session.dataTask(with: url) { (data, response, error) in
//            if let response = response {
////                print(response)
//            }
//            guard let data = data else { return }
//            let product: UserResponseWrapped = try! JSONDecoder().decode(UserResponseWrapped.self , from: data)
//            name = (product.response.first?.first_name)! + " " + (product.response.first?.last_name)!
//            photo = (product.response.first?.photo_100)!
//        }.resume()
//        sleep(1)
        
        let date = Date(timeIntervalSince1970: feedItem.date)
        let dateTitle = dateFormatter.string(from: date)
        
        let photoAttachment = self.photoAttachment(feedItem: feedItem)
        
        let sizes = cellLayoutCalculator.sizes(postText: getText(from: feedItem), photoAttachment: photoAttachment)
        owner_id = feedItem.owner_id
        return FeedViewModel.Cell.init(iconUrlString: photo, //сюда photo_100
                                       name: name,
                                       date: dateTitle,
                                       text: getText(from: feedItem),
                                       likes: formattedCounter(feedItem.likes?.count),
                                       comments: formattedCounter(feedItem.comments?.count),
                                       shares: formattedCounter(feedItem.reposts?.count),
                                       views: formattedCounter(feedItem.views?.count),
                                       photoAttachment: photoAttachment,
                                       sizes: sizes)
    }
    
//    func url(from path: String, params: [String: String]) -> URL {
//        var components = URLComponents()
//        components.scheme = API.scheme
//        components.host = API.host
//        components.path = path
//        components.queryItems = params.map{ URLQueryItem(name: $0, value: $1) }
//        return components.url!
//    }
    
    private func formattedCounter(_ counter: Int?) -> String? {
        guard let counter = counter, counter > 0 else { return nil }
        var counterString = String(counter)
        if 4...6 ~= counterString.count {
            counterString = String(counterString.dropLast(3)) + "K"
        } else if counterString.count > 6 {
            counterString = String(counterString.dropLast(6)) + "M"
        }
        return counterString
    }
    
    private func getText(from feedItem: FeedItem) -> String? {
        var text = feedItem.text!
        
        if feedItem.copy_history != nil {
            if !((feedItem.copy_history![0].text?.isEmpty)!) {
                if text.isEmpty {
                    text += "Repost:\n" + feedItem.copy_history![0].text!
                } else {
                    text += "\nRepost:\n" + feedItem.copy_history![0].text!
                }
            }
        }
        return text
    }
    
    private func photoAttachment(feedItem: FeedItem) -> FeedViewModel.FeedCellPhotoAttachment? {
        guard let photos = feedItem.copy_history?.first?.attachments?.flatMap({ (attachment) in
            attachment.photo
        }), let firstPhoto = photos.first else {
            return nil
        }
        return FeedViewModel.FeedCellPhotoAttachment.init(prhotoUrlString: firstPhoto.srcBIG, width: firstPhoto.width, height: firstPhoto.height)
    }

    
}
