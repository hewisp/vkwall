//
//  WallViewController.swift
//  VkWall
//
//  Created by Hewnon Wisp on 17/09/2019.
//  Copyright (c) 2019 Алексей Маринин. All rights reserved.
//

import UIKit

protocol WallDisplayLogic: class {
  func displayData(viewModel: Wall.Model.ViewModel.ViewModelData)
}

class WallViewController: UIViewController, WallDisplayLogic {

  var interactor: WallBusinessLogic?
  var router: (NSObjectProtocol & WallRoutingLogic)?
  
    private var feedViewModel = FeedViewModel.init(cells: [])
    
    @IBOutlet weak var table: UITableView!
    private var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        return refreshControl
    }()
    // MARK: Setup
  
  private func setup() {
    let viewController        = self
    let interactor            = WallInteractor()
    let presenter             = WallPresenter()
    let router                = WallRouter()
    viewController.interactor = interactor
    viewController.router     = router
    interactor.presenter      = presenter
    presenter.viewController  = viewController
    router.viewController     = viewController
  }
  
  // MARK: Routing
  

  
  // MARK: View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    setupTable()
    
    table.register(UINib(nibName: "WallCell", bundle: nil), forCellReuseIdentifier: WallCell.reuseId)
    view.backgroundColor = #colorLiteral(red: 0.5184490681, green: 1, blue: 0.7171438336, alpha: 1)
    interactor?.makeRequest(request: Wall.Model.Request.RequestType.getWallFedd)
    interactor?.makeRequest(request: Wall.Model.Request.RequestType.getUser)
  }
    
    private func setupTable() {
        let topInset: CGFloat = 8
        table.contentInset.top = topInset
        table.separatorStyle = .none
        table.backgroundColor = .clear
        
        interactor?.makeRequest(request: Wall.Model.Request.RequestType.getWallFedd)
        interactor?.makeRequest(request: Wall.Model.Request.RequestType.getUser)
        
        table.addSubview(refreshControl)
    }
    
    @objc private func refresh() {
        interactor?.makeRequest(request: Wall.Model.Request.RequestType.getWallFedd)
    }
  
  func displayData(viewModel: Wall.Model.ViewModel.ViewModelData) {
    
    switch viewModel {
    case .displayWall(let feedViewModel):
        self.feedViewModel = feedViewModel
        table.reloadData()
        refreshControl.endRefreshing()
    }
    }
}

func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    if scrollView.contentOffset.y > scrollView.contentSize.height / 1.1 {
        
    }
}

extension WallViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedViewModel.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: WallCell.reuseId, for: indexPath) as! WallCell
        let cellViewModel = feedViewModel.cells[indexPath.row]
        cell.set(viewModel: cellViewModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellViewModel = feedViewModel.cells[indexPath.row]
        return cellViewModel.sizes.totalHeight
    }
    
}
