//
//  AuthViewController.swift
//  VkWall
//
//  Created by Hewnon Wisp on 15/09/2019.
//  Copyright © 2019 Алексей Маринин. All rights reserved.
//

import UIKit

class AuthViewController: UIViewController {

    private var authService: AuthService!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        authService = AuthService()
        authService = AppDelegate.shared().authService
    }
    
    @IBAction func signinTouch() {
        authService.wakeUpSession()
    }
}
