//
//  String_height.swift
//  VkWall
//
//  Created by Hewnon Wisp on 19/09/2019.
//  Copyright © 2019 Алексей Маринин. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func height(width: CGFloat, font: UIFont) -> CGFloat {
//        let textSize = CGSize(width: width, height: .greatestFiniteMagnitude)
        let textSize = CGSize(width: width, height: .greatestFiniteMagnitude)
        
//        let size = self.boundingRect(with: textSize,
//                                     options: .usesLineFragmentOrigin,
//                                     attributes: [NSAttributedString.Key.font : font],
//                                     context: nil)
        let size = self.boundingRect(with: textSize,
                                     options: .usesLineFragmentOrigin,
//                                     attributes: [NSAttributedString.Key.font : font],
            attributes: [NSAttributedStringKey.font: font],
                                     context: nil)
        return ceil(size.height)
    }
}
