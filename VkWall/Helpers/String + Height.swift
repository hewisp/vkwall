//
//  String + Height.swift
//  VkWall
//
//  Created by Hewnon Wisp on 20/09/2019.
//  Copyright © 2019 Алексей Маринин. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func height(width: CGFloat, font: UIFont) -> CGFloat {
        let textSize = CGSize(width: width, height: .greatestFiniteMagnitude)
        let size = self.boundingRect(with: textSize,
                                     options: .usesLineFragmentOrigin,
            attributes: [NSAttributedStringKey.font: font],
            context: nil)
        return ceil(size.height)
    }
}

